
# Encoding for finding an admissible labeling for a 3-regular graph
# edgelist:     list of sets of size 2
# relators:     list of 3-tuples
# orientations: dictionary V -> E^3, with an entry for each vertex
def tripleLabeling_v2(edgelist, relators, orientations):
    variables = ["vertexLabel", "edgeLabel", "offset"]
    maps = dict(map(lambda v: (v, dict()), variables))
    nextIndex = [1]
    def toDimacs(clause):
        dimacs = []
        for lit in clause:
            name = lit[0]
            args = lit[1]
            pol  = lit[2]
            if maps[name].keys().isdisjoint({args}):
                maps[name].update([(args, nextIndex[0])])
                nextIndex[0] = nextIndex[0]+1
            dimacs.append(pol*maps[name][args])
        return dimacs

    vertices = set()
    for e in edgelist:
        vertices.update(e)
    tripleCount = len(relators)
    triples = range(tripleCount)

    labels = set()
    for t in relators:
        for e in t:
            labels.add(e)
    labelCount = len(labels)

    edgeCount = len(edgelist)
    edges = range(edgeCount)

    # Color graph and flip every other vertex
    cols = [{1}, set()]
    while cols[0].union(cols[1]) != vertices:
        for e in edgelist:
            if not e.isdisjoint(cols[0]):
                cols[1].update(e.difference(cols[0]))
            if not e.isdisjoint(cols[1]):
                cols[0].update(e.difference(cols[1]))

    labelOrientation = dict()
    for k, v in orientations.items():
        labelOrientation.update({k: v.copy()})
    for v in cols[1]:
        labelOrientation[v].reverse()

    # create clause generator
    # ------------------------------------------------------------------------
    # ------------------------------------------------------------------------
    def clauseGen():
        offsets = range(3)

        # --------------------------------------------------------
        # Each vertex should have exactly one label
        # --------------------------------------------------------
        # Each vertex has at most one label
        for v in vertices:
            for t1 in triples:
                for t2 in (t for t in triples if t > t1):
                    yield toDimacs([("vertexLabel", (v, t1), -1), ("vertexLabel", (v, t2), -1)])

        # Each vertex has at least one label
        for v in vertices:
            yield toDimacs([("vertexLabel", (v, t), 1) for t in triples])

        # --------------------------------------------------------
        # Each edge should have exactly one label
        # --------------------------------------------------------
        # Each edge has at most one label
        for e in edges:
            for l1 in labels:
                for l2 in (l for l in labels if l > l1):
                    yield toDimacs([("edgeLabel", (e, l1), -1), ("edgeLabel", (e, l2), -1)])

        # Each edge has at least one label
        for e in edges:
            yield toDimacs([("edgeLabel", (e, l), 1) for l in labels])

        # --------------------------------------------------------
        # Each vertex should have exactly one offset
        # --------------------------------------------------------
        # Each vertex has at most one offset
        for v in vertices:
            for o1 in offsets:
                for o2 in (o for o in offsets if o > o1):
                    yield toDimacs([("offset", (v, o1), -1), ("offset", (v, o2), -1)])

        # Each vertex has at least one offset
        for v in vertices:
            yield toDimacs([("offset", (v, o), 1) for o in offsets])

        # --------------------------------------------------------
        # Compatibility of labeling
        # --------------------------------------------------------
        # The triple given to a vertex determines the labels of incident edges
        for v in vertices:
            for o in offsets:
                for t in triples:
                    indices = list(map(lambda i: (i + o) % 3, range(3)))
                    incidentEdges = labelOrientation[v]
                    edgeLabels = list(map(lambda i: relators[t][i], indices))
                    for i in range(3):
                        e = incidentEdges[i]
                        l = edgeLabels[i]
                        yield toDimacs([("vertexLabel", (v, t), -1), ("offset", (v, o), -1), ("edgeLabel", (e, l), 1)])

        # Adjacent vertices may not be labeled with the same triple containing 3 different elements
        for e in edges:
            e_l = list(edgelist[e])
            v1, v2 = e_l[0], e_l[1]
            for t in triples:
                if len(set(relators[t])) == 3:
                    yield toDimacs([("vertexLabel", (v1, t), -1), ("vertexLabel", (v2, t), -1)])

        # If adjacent vertices have the same triple, their connecting edge must be labeled with the
        # element that has multiple instances in that triple. Additionally the index from which this label
        # is derived has to be different for both vertices.
        for e in edges:
            e_l = list(edgelist[e])
            v1, v2 = e_l[0], e_l[1]
            for t in triples:
                t_elems = relators[t]
                if len(set(t_elems)) == 2:
                    counts = dict(map(lambda x: (x, t_elems.count(x)), set(t_elems)))
                    for k, v in counts.items():
                        if v == 2:
                            elem = k
                    yield toDimacs([("vertexLabel", (v1, t), -1), ("vertexLabel", (v2, t), -1), ("edgeLabel", (e, elem), 1)])
                    v1_edge_ind = labelOrientation[v1].index(e)
                    v2_edge_ind = labelOrientation[v2].index(e)
                    for o1 in offsets:
                        for o2 in offsets:
                            v1_label_ind = (v1_edge_ind + o1) % 3
                            v2_label_ind = (v2_edge_ind + o2) % 3
                            if not (v1_label_ind != v2_label_ind and t_elems[v1_label_ind] == elem and t_elems[v2_label_ind] == elem):
                                yield toDimacs([("vertexLabel", (v1, t), -1), ("vertexLabel", (v2, t), -1), ("offset", (v1, o1), -1), ("offset", (v2, o2), -1)])

        # --------------------------------------------------------
        # Optimizations
        # --------------------------------------------------------
        # Adjacent vertices should not have triples with disjoint element set
        for e in edges:
            e_l = list(edgelist[e])
            v1 = e_l[0]
            v2 = e_l[1]
            for t1 in triples:
                for t2 in triples:
                    if set(relators[t1]).isdisjoint(set(relators[t2])):
                        yield toDimacs([("vertexLabel", (v1, t1), -1), ("vertexLabel", (v2, t2), -1)])

    # ------------------------------------------------------------------------
    # ------------------------------------------------------------------------

    # Function for interpreting a satisfying assignment
    def interpret(model):
        labelMap = maps["vertexLabel"]
        labelNums_ = maps["vertexLabel"].values()
        labelNums = set(filter(lambda x: abs(x) in labelNums_, model))
        vlabel = dict()
        for v in vertices:
            for l in triples:
                if labelMap[(v, l)] in labelNums:
                    vlabel.update({v:relators[l]})
        labelMap = maps["edgeLabel"]
        labelNums_ = maps["edgeLabel"].values()
        labelNums = set(filter(lambda x: abs(x) in labelNums_, model))
        elabel = dict()
        for e in edges:
            for l in labels:
                if labelMap[(e, l)] in labelNums:
                    elabel.update({e:l})
        offsetMap = maps["offset"]
        offsetNums_ = maps["offset"].values()
        offsetNums = set(filter(lambda x: abs(x) in offsetNums_, model))
        offset = dict()
        for v in vertices:
            for l in range(3):
                if offsetMap[(v, l)] in offsetNums:
                    offset.update({v:l})
        return vlabel, elabel, offset

    # return clause generator
    return clauseGen(), interpret

# Encoding representing a set of cycles in a 3-regular graph
# cycleCount:   number of cycles
# length:       length of a cycle
# edgelist:     graph as list of edges
def cycles_ud_orient_v2(cycleCount, length, edgelist):
    edgeCount = len(edgelist)*2 # number of edges in directed decomposition
    variables = ["orient", "inPath", "counter", "index", "counter2"]
    maps = dict(map(lambda v: (v, dict()), variables))
    nextIndex = [1]
    def toDimacs(clause):
        dimacs = []
        for lit in clause:
            name = lit[0]
            args = lit[1]
            pol  = lit[2]
            if maps[name].keys().isdisjoint({args}):
                maps[name].update([(args, nextIndex[0])])
                nextIndex[0] = nextIndex[0]+1
            dimacs.append(pol*maps[name][args])
        return dimacs

    vertices = set()
    for e in edgelist:
        vs = list(e)
        vertices.add(vs[0])
        vertices.add(vs[1])

    dedges = list(map(lambda e: tuple(e), edgelist))
    dedges = dedges + list(map(lambda e: (e[1], e[0]), dedges))
    nameToPair = dict(zip(range(edgeCount), dedges))
    orient0 = dict()
    orient1 = dict()
    for v in vertices:
        # Find inward edges
        inward = list(filter(lambda e: nameToPair[e][1] == v, range(edgeCount)))
        # And outward
        outward = list(map(lambda i: (i - len(edgelist)) % edgeCount, inward))

        or0 = [(inward[0], outward[1]), (inward[1], outward[2]), (inward[2], outward[0])]
        or1 = [(inward[0], outward[2]), (inward[2], outward[1]), (inward[1], outward[0])]
        orient0.update([(v, or0)])
        orient1.update([(v, or1)])

    dists_forward = list(map(lambda e: [{e}], range(edgeCount)))
    for k in range(7):
        for dl in dists_forward:
            dl.append(set())
        for k, v in nameToPair.items():
            for dl in dists_forward:
                for j in dl[len(dl) - 2]:
                    if v[0] == nameToPair[j][1]:
                        dl[len(dl) - 1].update({k})
    dists_backward = list(map(lambda e: [{e}], range(edgeCount)))
    for k in range(7):
        for dl in dists_backward:
            dl.append(set())
        for k, v in nameToPair.items():
            for dl in dists_backward:
                for j in dl[len(dl) - 2]:
                    if v[1] == nameToPair[j][0]:
                        dl[len(dl) - 1].update({k})

    allowed = list(map(lambda i: set(), range(edgeCount)))
    for e in range(edgeCount):
        for i, j in [(i, j) for i in range(8) for j in range(8) if i + j <= 8]:
            allowed[e].update(dists_forward[e][i].intersection(dists_backward[e][j]))
            allowed[e].update(dists_backward[e][i].intersection(dists_forward[e][j]))
    forbidden = list(map(lambda x: set(range(edgeCount)).difference(x), allowed))
    # These pairs of edges may not belong to the same cycle
    forbiddenPairs = {(i, j) for i in range(edgeCount) for j in forbidden[i]}

    # create clause generator
    # ------------------------------------------------------------------------
    # ------------------------------------------------------------------------
    def clauseGen():
        cycles = range(cycleCount)
        edges = range(edgeCount)
        indices = range(length)

        # --------------------------------------------------------
        # Each edge should be in exactly one path
        # --------------------------------------------------------
        # Each edge is in at most one path
        for e in edges:
            for p1 in cycles:
                for p2 in range(p1 + 1, cycleCount):
                    yield toDimacs([("inPath", (e, p1), -1), ("inPath", (e, p2), -1)])

        # Each edge is in at least one path
        for e in edges:
            yield toDimacs([("inPath", (e, p), 1) for p in cycles])

        # --------------------------------------------------------
        # Every edge has should have exactly one index
        # --------------------------------------------------------
        # Each edge has at most one index
        for e in edges:
            for i1 in indices:
                for i2 in range(i1 + 1, length):
                    yield toDimacs([("index", (e, i1), -1), ("index", (e, i2), -1)])

        # Each edge has some index
        for e in edges:
            yield toDimacs([("index", (e, i), 1) for i in range(length)])

        # --------------------------------------------------------
        # Every cycle has some edge
        # --------------------------------------------------------
        for c in cycles:
            yield toDimacs([("inPath", (e, c), 1) for e in edges])

        # --------------------------------------------------------
        # Every pair (cycle, index) belongs to at most one edge
        # --------------------------------------------------------
        for e1 in edges:
            for e2 in list(filter(lambda x: x > e1, edges)):
                for i in indices:
                    for c in cycles:
                        yield toDimacs([("inPath", (e1, c), -1), ("inPath", (e2, c), -1), ("index", (e1, i), -1), ("index", (e2, i), -1)])

        # --------------------------------------------------------
        # Orientation defines how inPath info is propagated
        # --------------------------------------------------------
        # Orientation 0
        for c in cycles:
            for v in vertices:
                for p in orient0[v]:
                    e1 = p[0]
                    e2 = p[1]
                    yield toDimacs([("orient", (v), 1), ("inPath", (e1, c), -1), ("inPath", (e2, c), 1)])
                    yield toDimacs([("orient", (v), 1), ("inPath", (e1, c), 1), ("inPath", (e2, c), -1)])
        # Orientation 1
        for c in cycles:
            for v in vertices:
                for p in orient1[v]:
                    e1 = p[0]
                    e2 = p[1]
                    yield toDimacs([("orient", (v), -1), ("inPath", (e1, c), -1), ("inPath", (e2, c), 1)])
                    yield toDimacs([("orient", (v), -1), ("inPath", (e1, c), 1), ("inPath", (e2, c), -1)])

        # --------------------------------------------------------
        # Orientation defines how index info is propagated
        # --------------------------------------------------------
        # Orientation 0
        for v in vertices:
            for i1 in range(length):
                for p in orient0[v]:
                    e1 = p[0]
                    e2 = p[1]
                    i2 = (i1 + 1) % length
                    yield toDimacs([("orient", (v), 1), ("index", (e1, i1), -1), ("index", (e2, i2), 1)])
                    yield toDimacs([("orient", (v), 1), ("index", (e1, i1), 1), ("index", (e2, i2), -1)])
        # Orientation 1
        for v in vertices:
            for i1 in range(length):
                for p in orient1[v]:
                    e1 = p[0]
                    e2 = p[1]
                    i2 = (i1 + 1) % length
                    yield toDimacs([("orient", (v), -1), ("index", (e1, i1), -1), ("index", (e2, i2), 1)])
                    yield toDimacs([("orient", (v), -1), ("index", (e1, i1), 1), ("index", (e2, i2), -1)])

        # --------------------------------------------------------
        # Symmetry-breaking constraints
        # --------------------------------------------------------
        # The least edge in a cycle has the least index
        for e1 in vertices:
            for e2 in list(filter(lambda x: x > e1, vertices)):
                for i1 in [0]:
                    for i2 in list(filter(lambda x: x > i1, indices)):
                        for c in cycles:
                            yield toDimacs([("inPath", (e1, c), -1), ("inPath", (e2, c), -1), ("index", (e2, i1), -1), ("index", (e1, i2), -1)])

        # The cycles are ordered in increasing order wrt the edge at the least index
        for e1 in vertices:
            for e2 in list(filter(lambda x: x > e1, vertices)):
                for c1 in cycles:
                    for c2 in list(filter(lambda x: x > c1, cycles)):
                        yield toDimacs([("inPath", (e1, c2), -1), ("inPath", (e2, c1), -1), ("index", (e2, 0), -1), ("index", (e1, 0), -1)])

        # --------------------------------------------------------
        # Optimizations
        # --------------------------------------------------------
        # Edges that are too far apart cannot belong to the same cycle
        for e1, e2 in forbiddenPairs:
            for c in cycles:
                yield toDimacs([("inPath", (e1, c), -1), ("inPath", (e2, c), -1)])

    # ------------------------------------------------------------------------
    # ------------------------------------------------------------------------

    # Create function for generating blocking clauses from models
    def blockingClause(model):
        orientNums = set(maps["orient"].values())
        orientations = list(filter(lambda i: abs(i) in orientNums, model))
        bl_clause = list(map(lambda i: -i, orientations))
        return bl_clause

    # Create function that maps model to orientations
    def getOrientation(model):
        truevars = set(filter(lambda i: i > 0, model))
        v_orient_1 = []
        for v in vertices:
            if maps["orient"][v] in truevars:
                v_orient_1.append(v)
        orientation_edgepairs = orient0.copy()
        for v in v_orient_1:
            orientation_edgepairs.update([(v, orient1[v])])
        orientation = dict()
        for k, v in orientation_edgepairs.items():
            vertex_triple = [v[0][0],
                             v[0][1],
                             v[1][1]]
            vertex_triple = list(map(lambda x: x % len(edgelist), vertex_triple))
            orientation.update({k: vertex_triple})
        return orientation

    # return clause generator & utility functions
    return clauseGen(), blockingClause, getOrientation