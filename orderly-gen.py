import array
import time

# An exhaustive isomorph-free generator for sets of N 8-cycles of connected, 3-regular, bipartite graphs.
class Cyclegen():
    # HOX: * cycleCount should be at most 32
    #      * initConf should be given as an array
    def __init__(self, cycleCount, initConf, printToFile=False, outputLocation="", timeout=-1):
        self.timeout = timeout
        self.timed_out = False
        self.confCount = 0
        # Initialize data structures
        self.printToFile = printToFile
        self.outputLocation = outputLocation
        if printToFile:
            self.out_stream = open(self.outputLocation, "ab")
        self.cycleCount = cycleCount
        self.edgeCount = cycleCount * 8
        self.initLength = len(initConf)
        self.length = 0
        self.maxLength = cycleCount * 8
        self.leastFreeEdge = 0
        self.greatestEdgeInConf = 0
        self.greatestCycleInConf = 0
        self.candidateCount = array.array('i', map(lambda x: 0, range(self.maxLength + 1)))
        # -- Reserve memory for arrays
        self.currentConf = array.array('h', map(lambda x: -1, range(self.maxLength)))
        self.testConf = array.array('h', map(lambda x: -1, range(self.maxLength * 2)))
        self.usedEdges = array.array('h', map(lambda x: -1, range(self.cycleCount * 8)))
        self.isAttachedToSelf = array.array('b', map(lambda x: 0, range(self.cycleCount)))
        self.occurrencesOfCycle = array.array('b', map(lambda x: 0, range(self.cycleCount)))
        self.coOccurrencesOfCycles = array.array('h', map(lambda x: 0, range(self.cycleCount ** 2)))
        self.degreeOfEdge = list(map(lambda x: array.array('b', [1]), range(self.cycleCount * 8)))
        self.reserved_q = array.array('h', map(lambda x: -1, range(self.cycleCount * 8)))
        self.reservedAt = array.array('h', map(lambda x: -1, range(self.cycleCount * 8)))
        self.prevNeighbour = array.array('h', map(lambda x: -1, range(self.cycleCount * 8)))
        self.nextNeighbour = array.array('h', map(lambda x: -1, range(self.cycleCount * 8)))
        #####
        self.stateStack = dict(map(lambda i: (i, None), range(0, self.edgeCount, 2)))
        self.confs = list()
        #####
        # copy initial configuration and initialize data structures
        i = 0
        while i < len(initConf):
            # Copy an edge pair
            self.currentConf[i] = initConf[i]
            i += 1
            self.currentConf[i] = initConf[i]
            i += 1
            # Update data structures
            self.addEdgePair()

        return

    def getState(self):
        degs = list(range(len(self.degreeOfEdge)))
        for i in range(len(self.degreeOfEdge)):
            degs[i] = self.degreeOfEdge[i][0]
        return [self.leastFreeEdge, self.greatestEdgeInConf, self.greatestCycleInConf,
                list(self.usedEdges), list(self.isAttachedToSelf), list(self.occurrencesOfCycle),
                list(self.coOccurrencesOfCycles), degs.copy(), list(self.reservedAt), list(self.reserved_q),
                list(self.prevNeighbour), list(self.nextNeighbour)]

    # Updates auxiliary data structures when an edge-pair is added
    #   -- Assumes that length has not yet been updated.
    #   -- New edge pair should already be written into the array.
    def addEdgePair(self):
        #######
        #self.stateStack[self.length] = self.getState()
        #######
        oldLength = self.length
        e1 = self.currentConf[oldLength]
        e2 = self.currentConf[oldLength + 1]
        c1 = e1 // 8
        c2 = e2 // 8
        i1 = e1 % 8
        i2 = e2 % 8
        e1_prev = c1 * 8 + ((i1 - 1) % 8)
        e1_next = c1 * 8 + ((i1 + 1) % 8)
        e2_prev = c2 * 8 + ((i2 - 1) % 8)
        e2_next = c2 * 8 + ((i2 + 1) % 8)
        e1_prevNeighbour = self.prevNeighbour[e1]
        e2_prevNeighbour = self.prevNeighbour[e2]
        e1_nextNeighbour = self.nextNeighbour[e1]
        e2_nextNeighbour = self.nextNeighbour[e2]
        # Update:
        #   -- length
        self.length += 2
        #   -- usedEdges
        self.usedEdges[e1] = oldLength
        self.usedEdges[e2] = oldLength + 1
        #   -- leastFreeEdge
        while True:
            self.leastFreeEdge += 1
            if self.leastFreeEdge >= self.length or self.usedEdges[self.leastFreeEdge] < 0:
                break
        #   -- greatestEdge
        if e1 > self.greatestEdgeInConf:
            self.greatestEdgeInConf = e1
        if e2 > self.greatestEdgeInConf:
            self.greatestEdgeInConf = e2
        #   -- greatestCycle
        self.greatestCycleInConf = self.greatestEdgeInConf // 8
        #   -- isAttachedToSelf
        if c1 == c2:
            self.isAttachedToSelf[c1] = 1
        #   -- occurrencesOfCycle
        self.occurrencesOfCycle[c1] += 1
        if c1 != c2:
            self.occurrencesOfCycle[c2] += 1
        #   -- coOccurencesOfCycles
        i = c1*self.cycleCount + c2
        self.coOccurrencesOfCycles[i] += 1
        #   -- degreeOfEdge
        if e1_prevNeighbour < 0:
            # First edge has neither prev or next neighbour
            # ==> Increment and meld vertices
            self.degreeOfEdge[e1][0] += 1
            self.degreeOfEdge[e2_next] = self.degreeOfEdge[e1]
            self.degreeOfEdge[e2][0] += 1
            self.degreeOfEdge[e1_next] = self.degreeOfEdge[e2]
        elif self.nextNeighbour[e1] < 0:
            # First edge has prev neighbour but not next
            self.degreeOfEdge[e1][0] += 1
            if self.usedEdges[e2_next] < 0:
                self.degreeOfEdge[e2_next] = self.degreeOfEdge[e1]
            self.degreeOfEdge[e2][0] += 1
            self.degreeOfEdge[e1_next] = self.degreeOfEdge[e2]
        else:
            # First edge has both prev and next neighbour
            self.degreeOfEdge[e1][0] += 1
            self.degreeOfEdge[e1_next][0] += 1
            if self.usedEdges[e2_next] < 0:
                self.degreeOfEdge[e2_next] = self.degreeOfEdge[e1]
            if self.usedEdges[e2_prev] < 0:
                self.degreeOfEdge[e2] = self.degreeOfEdge[e1_next]
        #   -- prevNeighbour & nextNeighbour
        #   -- reserved_q
        self.prevNeighbour[e2_next] = e1_prev
        self.nextNeighbour[e1_prev] = e2_next
        self.prevNeighbour[e1_next] = e2_prev
        self.nextNeighbour[e2_prev] = e1_next
        if e1_prevNeighbour < 0:
            help_me = 1337
        elif self.nextNeighbour[e1] < 0:
            # First edge has prevNeighbour, but not nextNeighbour
            if self.usedEdges[e2_next] < 0:
                self.reserved_q[e2_next] = e1_prevNeighbour
                self.reserved_q[e1_prevNeighbour] = e2_next
                if self.reservedAt[e2_next] < 0:
                    self.reservedAt[e2_next] = oldLength
                    self.reservedAt[e1_prevNeighbour] = oldLength
            if e2_prevNeighbour < 0:
                # Other edge does not have prevNeighbour
                self.prevNeighbour[e1_next] = e2_prev
                self.nextNeighbour[e2_prev] = e1_next
            else:
                # Other edge has prevNeighbour
                self.reserved_q[e2_prevNeighbour] = e1_next
                self.reserved_q[e1_next] = e2_prevNeighbour
                if self.reservedAt[e2_prevNeighbour] < 0:
                    self.reservedAt[e2_prevNeighbour] = oldLength
                    self.reservedAt[e1_next] = oldLength
        else:
            # First edge has both prev & next neighbours
            e1_nextNeighbour = self.nextNeighbour[e1]
            if self.usedEdges[e2_prev] < 0:
                self.reserved_q[e2_prev] = e1_nextNeighbour
                self.reserved_q[e1_nextNeighbour] = e2_prev
                if self.reservedAt[e2_prev] < 0:
                    self.reservedAt[e2_prev] = oldLength
                    self.reservedAt[e1_nextNeighbour] = oldLength
            if self.usedEdges[e2_next] < 0:
                self.reserved_q[e2_next] = e1_prevNeighbour
                self.reserved_q[e1_prevNeighbour] = e2_next
                if self.reservedAt[e2_next] < 0:
                    self.reservedAt[e2_next] = oldLength
                    self.reservedAt[e1_prevNeighbour] = oldLength
        return

    # Updates auxiliary data structures when an edge-pair is removed
    def removeEdgePair(self):
        # Remove old entries
        if self.length < self.maxLength:
            self.currentConf[self.length] = -1
            self.currentConf[self.length + 1] = -1
        # Update:
        #   -- length
        self.length -= 2
        e1 = self.currentConf[self.length]
        e2 = self.currentConf[self.length + 1]
        c1 = e1 // 8
        c2 = e2 // 8
        i1 = e1 % 8
        i2 = e2 % 8
        e1_prev = c1 * 8 + ((i1 - 1) % 8)
        e1_next = c1 * 8 + ((i1 + 1) % 8)
        e2_prev = c2 * 8 + ((i2 - 1) % 8)
        e2_next = c2 * 8 + ((i2 + 1) % 8)
        e1_prevNeighbour = self.prevNeighbour[e1]
        e2_prevNeighbour = self.prevNeighbour[e2]
        e1_nextNeighbour = self.nextNeighbour[e1]
        e2_nextNeighbour = self.nextNeighbour[e2]
        #   -- usedEdges
        self.usedEdges[e1] = -1
        self.usedEdges[e2] = -1
        #   -- leastFreeEdge
        self.leastFreeEdge = e1
        #   -- greatestEdge
        if self.greatestEdgeInConf == e2:
            while self.greatestEdgeInConf > 0:
                self.greatestEdgeInConf -= 1
                if self.usedEdges[self.greatestEdgeInConf] >= 0:
                    break
        #   -- greatestCycle
        self.greatestCycleInConf = self.greatestEdgeInConf // 8
        #   -- isAttachedToSelf
        if c1 == c2:
            self.isAttachedToSelf[c1] = 0
        #   -- occurrencesOfCycle
        self.occurrencesOfCycle[c1] -= 1
        if c1 != c2:
            self.occurrencesOfCycle[c2] -= 1
        #   -- coOccurencesOfCycles
        i = c1*self.cycleCount + c2
        self.coOccurrencesOfCycles[i] -= 1
        #   -- degreeOfEdge
        if e1_prevNeighbour < 0:
            if e1_nextNeighbour < 0:
                # First edge has no prev or next neighbour
                # ==> Separate vertices and set deg to 1
                self.degreeOfEdge[e2_next][0] -= 1
                self.degreeOfEdge[e1] = array.array('b', [1])
                self.degreeOfEdge[e2][0] -= 1
                self.degreeOfEdge[e1_next] = array.array('b', [1])
            else:
                # First edge has next neighbour but not prev
                self.degreeOfEdge[e1_next][0] -= 1
                if self.usedEdges[e2_prev] < 0:
                    self.degreeOfEdge[e2] = array.array('b', [1])
                self.degreeOfEdge[e2_next][0] -= 1
                self.degreeOfEdge[e1] = array.array('b', [1])
        elif self.nextNeighbour[e1] < 0:
            # First edge has prevNeighbour, but not nextNeighbour
            if e2_prevNeighbour < 0:
                # Other edge does not have prevNeighbour
                # ==> separate e1_next and e2 + set deg to 1
                # ==> decrement deg of e1
                # ==> if e2_next is not attached to e1_prevNeighbour, separate and set e2_next to 1
                self.degreeOfEdge[e1_next][0] -= 1
                self.degreeOfEdge[e2] = array.array('b', [1])
                self.degreeOfEdge[e1][0] -= 1
                if self.usedEdges[e2_next] < 0:
                    self.degreeOfEdge[e2_next] = array.array('b', [1])
            else:
                # Other edge has prevNeighbour
                # ==> decrement e2
                # ==> separate e1_next and e2 + set e1_next to 1
                # ==> decrement e1
                # ==> if e2_next is not attached to e1_prevNeighbour, separate and set e2_next to 1
                self.degreeOfEdge[e2][0] -= 1
                self.degreeOfEdge[e1_next] = array.array('b', [1])
                self.degreeOfEdge[e1][0] -= 1
                if self.usedEdges[e2_next] < 0:
                    self.degreeOfEdge[e2_next] = array.array('b', [1])
        else:
            # First edge has both prev & next neighbours
            # ==> decrement e1_next and e1
            # ==> If e2_next not attached, separate e2_next and set to 1
            # ==> If e2_prev not attached, separate e2 and set to 1
            self.degreeOfEdge[e1][0] -= 1
            self.degreeOfEdge[e1_next][0] -= 1
            if self.usedEdges[e2_next] < 0:
                self.degreeOfEdge[e2_next] = array.array('b', [1])
            if self.usedEdges[e2_prev] < 0:
                self.degreeOfEdge[e2] = array.array('b', [1])

        #   -- prevNeighbour & nextNeighbour
        #   -- reserved_q
        self.prevNeighbour[e2_next] = -1
        self.prevNeighbour[e1_next] = -1
        self.nextNeighbour[e1_prev] = -1
        self.nextNeighbour[e2_prev] = -1
        if e1_prevNeighbour < 0:
            # First edge has no prev or next neighbour
            # ==> remove neighbour entries
            help_me2 = 65
        elif self.nextNeighbour[e1] < 0:
            # First edge has prevNeighbour, but not nextNeighbour
            if self.reservedAt[e2_next] >= self.length:
                self.reserved_q[self.reserved_q[e2_next]] = -1
                self.reservedAt[self.reserved_q[e2_next]] = -1
                self.reserved_q[e2_next] = -1
                self.reservedAt[e2_next] = -1
            if e2_prevNeighbour < 0:
                # Other edge does not have prevNeighbour
                # ==> remove neighbour entries
                self.prevNeighbour[e1_next] = -1
                self.nextNeighbour[e2_prev] = -1
            else:
                # Other edge has prevNeighbour
                # ==> remove outdated reserved_q entries
                if self.reservedAt[e1_next] >= self.length:
                    self.reserved_q[self.reserved_q[e1_next]] = -1
                    self.reservedAt[self.reserved_q[e1_next]] = -1
                    self.reserved_q[e1_next] = -1
                    self.reservedAt[e1_next] = -1
        else:
            # First edge has both prev & next neighbours
            # ==> remove outdated reserved_q entries
            if self.reservedAt[e2_next] >= self.length:
                self.reserved_q[self.reserved_q[e2_next]] = -1
                self.reservedAt[self.reserved_q[e2_next]] = -1
                self.reserved_q[e2_next] = -1
                self.reservedAt[e2_next] = -1
            if self.reservedAt[e2_prev] >= self.length:
                self.reserved_q[self.reserved_q[e2_prev]] = -1
                self.reservedAt[self.reserved_q[e2_prev]] = -1
                self.reserved_q[e2_prev] = -1
                self.reservedAt[e2_prev] = -1
        return

    # This function tries to construct the minimal possible configuration given knowledge of which cycle should map to
    # 0 and what its offset should be. Returns True, if the minimized conf is smaller than the current conf and
    # False if the minimized conf is equal or greater.
    def canonize(self, cycle0, offset0):
        # 1. Initialize testconf by mapping 'cycle0' to 0 with offset 'offset0'
        # 2. Repeat:
        #       -- update testconf
        #       -- if testconf < conf ==> return True
        #          if testconf > conf ==> return False
        #       -- if testconf is complete ==> return False
        #       -- if testconf is not complete ==> find next cycle and offset
        cycle_from = cycle0
        cycle_to = 0
        offset = offset0
        done_until = 0
        same_until = 0
        touched_until = 0
        ready_indices = array.array('i', map(lambda x: -1, range(self.length)))
        isEdgeUsed = array.array('i', map(lambda x: -1, range(8)))  # -1 = no, otherwise yes
        # Clean testconf
        for k in range(self.length):
            self.testConf[k] = -1
        while True:
            # print(f"Cycle {cycle_from} mapping to {cycle_to} with offset {offset}")
            # print(f"same until: {same_until}")
            # print(f"done until: {done_until}")
            # print(f"touched until: {touched_until}")
            # print(self.currentConf)
            # print(self.testConf)
            # if self.currentConf[:same_until] != self.testConf[:same_until]:
            #     raise Exception(f"Problem with same_until!!!!")
            # if offset % 2 == 1:
            #     raise Exception(f"Offset is not even!!!!")
            #####################################################################################
            # Update testconf
            #       Update existing incomplete entries, namely indices [same_until, touched_until)
            #       Map rest of the entries in order
            #           Update touched_until
            #       Update done_until
            #       Update same_until
            #           If difference is found, return True/False appropriately
            # Check whether testconf is complete
            #       If it is, return False
            #       If it is not, find next cycle and offset
            ######################################################################################

            # Check which edges from cycle 'cycle_from' are actually in the conf
            for k in range(8):
                if self.usedEdges[cycle_from * 8 + k] >= 0:
                    isEdgeUsed[k] = -1
                else:
                    isEdgeUsed[k] = 1
            ##print(isEdgeUsed)
            ##print(f"same: {same_until}\ndone: {done_until}\ntouched: {touched_until}")
            # Iterate through incomplete entries and permute if edge from 'cycle_from' is found
            ##print(f"from {((same_until // 2) * 2) + 1} until {touched_until}")
            for k in range(((same_until // 2) * 2) + 1, touched_until, 2):
                ##print(f"k: {k}")
                e = self.testConf[k]
                c = e // 8
                i = e % 8
                if ready_indices[k] < 0 and c == cycle_from:
                    isEdgeUsed[i] = 1
                    self.testConf[k] = cycle_to * 8 + ((i + offset) % 8)
                    ready_indices[k] = 1
            # Map rest of edges of 'cycle_from' (if any)
            for forw_ind in range(8):
                rev_ind = (forw_ind - offset) % 8
                if isEdgeUsed[rev_ind] < 0:
                    ##print(f"rev_ind: {rev_ind}")
                    # edge at this index not used yet but exists in conf
                    # ==> map it to next available slot
                    e_in_conf = cycle_from * 8 + rev_ind
                    loc_in_conf = self.usedEdges[e_in_conf]
                    pair_loc_in_conf = (loc_in_conf // 2) * 2 + 1 - (loc_in_conf % 2)
                    pair_e_in_conf = self.currentConf[pair_loc_in_conf]
                    pair_c = pair_e_in_conf // 8
                    pair_i = pair_e_in_conf % 8
                    ##print(f"e_in_conf: {e_in_conf}")
                    ##print(f"pair_in_conf: {pair_e_in_conf}")
                    ##print(touched_until)
                    # Check if this pair is a self-attachment
                    if pair_c == cycle_from:
                        # Pair is a self-attachment
                        # ==> Permute both edges in pair
                        new_e1 = cycle_to * 8 + forw_ind
                        new_e2 = cycle_to * 8 + ((pair_i + offset) % 8)
                        self.testConf[touched_until] = new_e1
                        self.testConf[touched_until + 1] = new_e2
                        isEdgeUsed[rev_ind] = 1
                        isEdgeUsed[pair_i] = 1
                        ready_indices[touched_until] = 1
                        ready_indices[touched_until + 1] = 1
                    else:
                        # Pair is not self-attachment
                        # ==> Only permute first edge
                        new_e1 = cycle_to * 8 + forw_ind
                        new_e2 = pair_e_in_conf
                        self.testConf[touched_until] = new_e1
                        self.testConf[touched_until + 1] = new_e2
                        isEdgeUsed[rev_ind] = 1
                        ready_indices[touched_until] = 1
                    touched_until += 2
            # Update done_until
            while True:
                if done_until >= self.length or ready_indices[done_until] < 0:
                    break
                else:
                    done_until += 1
            # Update same_until
            while True:
                if same_until == done_until:
                    break
                if self.testConf[same_until] < self.currentConf[same_until]:
                    return True
                elif self.testConf[same_until] > self.currentConf[same_until]:
                    return False
                else:  # equal entries
                    same_until += 1
            # Check if testconf is complete
            if done_until == self.length:
                return False
            else:
                cycle_to += 1
                # Find next cycle to map
                cycle_from = self.testConf[done_until] // 8
                offset = 8 - (self.testConf[done_until] % 8)
                if self.testConf[done_until - 1] % 2 == 0:
                    offset += 1

    # Checks whether current configuration is canonical
    def isCanonical(self):
        # Iterate thru cycles 1-MAX
        #   if cycle has less self-attachments than cycle 0, continue
        #   Iterate thru possible offsets for chosen cycle
        #       HOX: If cycle 0 has self-attachment, there is only one possible offset.
        #            Otherwise there might be several possible offsets.
        #            Perhaps iterate thru all offsets for now?
        #       Iteratively build testconf until a difference is found or it is completed and equal.
        for cycle0 in range(0, self.greatestCycleInConf + 1):
            if self.occurrencesOfCycle[cycle0] < 1:
                continue
            if self.isAttachedToSelf[0] == 1:
                # 0 has self attachment
                # ==> any cycle mapping to 0 must also have self-attachment
                if self.isAttachedToSelf[cycle0] < 1:
                    continue
                # Compute correct offset to get (0, 3) or (0, 5) as first pair
                e1 = None
                e2 = None
                for e1 in range(cycle0 * 8, cycle0 * 8 + 8):
                    loc1 = self.usedEdges[e1]
                    if loc1 < 0:
                        continue
                    e2 = self.currentConf[loc1 + 1 - 2 * (loc1 % 2)]
                    if e1 // 8 == e2 // 8:
                        break
                i1 = e1 % 8
                i2 = e2 % 8
                offset = None
                if i1 % 2 == 0:
                    offset = -i1
                else:
                    offset = -i2
                if self.canonize(cycle0, offset):
                    return False
            else:
                # 0 has no self-attachments
                # Try all offsets?
                for offset in range(0, 8, 2):
                    if self.canonize(cycle0, offset):
                        return False
        return True

    # Checks whether e2 can be attached to e1.
    def checkPointers(self, e1, e2):
        c1 = e1 // 8
        c2 = e2 // 8
        i1 = e1 % 8
        i2 = e2 % 8
        e1_prev = c1 * 8 + ((i1 - 1) % 8)
        e1_next = c1 * 8 + ((i1 + 1) % 8)
        e2_prev = c2 * 8 + ((i2 - 1) % 8)
        e2_next = c2 * 8 + ((i2 + 1) % 8)
        e1_prevNeighbour = self.prevNeighbour[e1]
        e2_prevNeighbour = self.prevNeighbour[e2]
        e1_nextNeighbour = self.nextNeighbour[e1]
        e2_nextNeighbour = self.nextNeighbour[e2]
        if e1_prevNeighbour < 0:
            # First edge has no neighbours
            return True
        elif self.nextNeighbour[e1] < 0:
            # First edge has prevNeighbour, but not nextNeighbour
            if e2_nextNeighbour >= 0 and \
                    e2_nextNeighbour != (e1_prevNeighbour // 8) * 8 + ((e1_prevNeighbour % 8 + 1) % 8):
                # print(e2_nextNeighbour)
                # print((e1_prevNeighbour // 8) * 8 + ((e1_prevNeighbour % 8 + 1) % 8))
                # print(3)
                return False
            if self.reserved_q[e2_next] >= 0 and self.reserved_q[e2_next] != e1_prevNeighbour:
                return False
            if self.reserved_q[e1_prevNeighbour] >= 0 and self.reserved_q[e1_prevNeighbour] != e2_next:
                return False
            if e2_prevNeighbour >= 0 and self.usedEdges[e2_prevNeighbour] >= 0:
                return False
            if e2_prevNeighbour >= 0 and self.reserved_q[e2_prevNeighbour] >= 0 and self.reserved_q[e2_prevNeighbour] != e1_next:
                return False
            if e2_prevNeighbour >= 0 and self.reserved_q[e1_next] >= 0 and self.reserved_q[e1_next] != e2_prevNeighbour:
                return False
            return True
        else:
            # First edge has both prev & next neighbours
            if e2_nextNeighbour >= 0 and \
                    e2_nextNeighbour != (e1_prevNeighbour // 8) * 8 + ((e1_prevNeighbour % 8 + 1) % 8):
                return False
            if self.reserved_q[e2_next] >= 0 and self.reserved_q[e2_next] != e1_prevNeighbour:
                return False
            if self.reserved_q[e1_prevNeighbour] >= 0 and self.reserved_q[e1_prevNeighbour] != e2_next:
                return False
            if e2_prevNeighbour >= 0 and e2_prevNeighbour != (e1_nextNeighbour // 8) * 8 + ((e1_nextNeighbour % 8 - 1) % 8):
                return False
            if self.reserved_q[e2_prev] >= 0 and self.reserved_q[e2_prev] != e1_nextNeighbour:
                return False
            if self.reserved_q[e1_nextNeighbour] >= 0 and self.reserved_q[e1_nextNeighbour] != e2_prev:
                return False
            return True

    # Augments the current configuration
    def augment(self):
        # 1. set e1 := least free edge
        #        -- If e1 >= edgeCount, return false (reached maximum size)
        # 2. find next suitable free edge.
        #        -- Check if current conf is "complete". If it is, do not augment since that would
        #           create a disconnected conf.
        #        -- Check whether e1 is reserved. If it is, attach to that edge.
        #        -- Can start from old edge in case of backtracking. Maybe use -1's in the conf array to indicate no
        #           backtracking.
        #        -- Check if c1 has "self-edge". If it does, start at the next cycle. If it doesn't,
        #           try (c1, i1 + 3) and (c1, i1 + 5).
        #        -- Iterate in increments of 2 (Bipartiteness)
        # 3. write new values to conf array
        # 4. update auxiliary data

        # 1.
        e1 = self.leastFreeEdge
        c1 = e1 // 8
        i1 = e1 % 8
        if e1 >= self.edgeCount:
            return False
        # 2.
        # Check completeness
        if self.greatestEdgeInConf % 8 == 7 and e1 > self.greatestEdgeInConf:
            return False
        # Check reservedness
        e2 = self.reserved_q[e1]
        if e2 >= 0:
            c2 = e2 // 8
            i2 = e2 % 8
            # Checks for self-attachments
            if c1 == c2 and c1 == 0 and self.length != 0:
                return False
            if c1 == c2 and c1 != 0 and self.isAttachedToSelf[0] == 0:
                return False
            # Some other checks
            if not (self.currentConf[self.length] < 0 and self.checkPointers(e1, e2)):
                return False
            if c1 == c2 and (i2 - i1 == 1 or i2 - i1 == 7):
                return False
            self.currentConf[self.length] = e1
            self.currentConf[self.length + 1] = e2
            self.addEdgePair()
            return True
        # Check whether backtracking happened
        e1_old = self.currentConf[self.length]
        if e1_old < 0:
            # Backtracking did not happen. Set starting state.
            # Check whether cycle of e1 is attached to itself
            if self.isAttachedToSelf[c1] > 0:
                # c1 is attached to itself
                # ==> Cannot add another self-attachment
                # ==> Start at next cycle
                c2 = c1 + 1
                i2 = 1 - (i1 % 2)
            else:
                # c1 is not attached to itself
                # ==> Check if cycle 0 is attached to itself
                #     If it is not, no other edges may be attached to themselves
                if i1 <= 4 and (self.isAttachedToSelf[0] == 1 or (c1 == 0 and self.length == 0)):
                    c2 = c1
                    i2 = i1 + 3
                else:
                    c2 = c1 + 1
                    i2 = 1 - (i1 % 2)
            e2 = c2 * 8 + i2
        else:
            # Backtracking happened. Continue as normal.
            e2 = self.currentConf[self.length + 1] + 2
        # Iterate until a feasible pair is found or run out of edges to try
        while True:
            c2 = e2 // 8
            i2 = e2 % 8
            # Check if this e2 is below max
            if e2 >= self.edgeCount:
                return False
            # Check if this e2 is too high
            if e2 > (self.greatestCycleInConf + 1) * 8 + 1:
                # print(f"Skipping {(e1, e2)} for being too high")
                return False
            # Check if this e2 is feasible
            #   -- Not used
            #   -- Not reserved for another edge
            #   -- Not a cusp
            #   -- Not breaking degree constraints
            if self.usedEdges[e2] >= 0:
                # print(f"Skipping {(e1, e2)} for usedness")
                e2 += 2
                continue
            if self.reserved_q[e2] >= 0:
                # print(f"Skipping {(e1, e2)} for reservedness")
                e2 += 2
                continue
            if c1 == c2 and (i2 - i1 == 1 or i2 - i1 == 7):
                # print(f"Skipping {(e1, e2)} for cusp violation")
                e2 += 2
                continue
            #if self.degreeOfEdge[c2 * 8 + ((i2 + 1) % 8)][0] > 1:
            #    print(f"Skipping {(e1, e2)} for degree violation")
            #    e2 += 2
            #    continue
            if not self.checkPointers(e1, e2):
                # print(f"Skipping {(e1, e2)} for pointer violation")
                e2 += 2
                continue
            # e2 passed all feasibility checks ==> add pair
            self.currentConf[self.length] = e1
            self.currentConf[self.length + 1] = e2
            self.addEdgePair()
            return True

    # "Removes" one element and updates the auxiliary data
    def backtrack(self):
        self.removeEdgePair()
        # if self.length >= 0 and self.stateStack[self.length] != self.getState():
        #     raise Exception(f"Backtracking failed!!!!!")

    # Breaking condition for main loop
    def breakCondition(self):
        return self.length < self.initLength

    def output(self):
        #self.candidateCount[self.length] += 1
        #if self.length == self.maxLength and self.isCanonical():
        #    self.confs.append(list(self.currentConf))
        if self.length == self.maxLength and self.isCanonical():
            self.confCount += 1
            if self.printToFile:
                self.out_stream.write(bytes(self.currentConf))


    def checkConf(self):
        self.checkDuplicateEdges()
        self.checkEdgeOrderAndUsedEdges()
        self.checkGreatestEdgeAndCycleInConf()
        self.checkLeastFreeEdgeInConf()
        self.checkOccurrencesOfCycles()
        #self.checkNextNeighbour()
        #self.checkPrevNeighbour()
        #self.checkReservedAt()
        #self.checkReserved_q()
        self.checkIsAttachedToSelf()
        self.checkDegrees()
        return

    def checkDegrees(self):
        deg = dict()
        e_to_v = dict()
        least_free = 0
        for j1 in range(0, self.length, 2):
            j2 = j1 + 1
            e1 = self.currentConf[j1]
            e2 = self.currentConf[j2]
            c1 = e1 // 8
            c2 = e2 // 8
            i1 = e1 % 8
            i2 = e2 % 8
            e1_next = c1 * 8 + ((i1 + 1) % 8)
            e2_next = c2 * 8 + ((i2 + 1) % 8)
            if e1 not in e_to_v.keys() and e2_next not in e_to_v.keys():
                e_to_v.update({e1: least_free, e2_next: least_free})
                deg.update({least_free: 2})
                least_free += 1
            elif e2_next not in e_to_v.keys():
                e_to_v.update({e2_next: e_to_v[e1]})
                deg[e_to_v[e1]] += 1
            else:
                e_to_v.update({e1: e_to_v[e2_next]})
                deg[e_to_v[e2_next]] += 1
            if e2 not in e_to_v.keys() and e1_next not in e_to_v.keys():
                e_to_v.update({e2: least_free, e1_next: least_free})
                deg.update({least_free: 2})
                least_free += 1
            elif e1_next not in e_to_v.keys():
                e_to_v.update({e1_next: e_to_v[e2]})
                deg[e_to_v[e2]] += 1
            else:
                e_to_v.update({e2: e_to_v[e1_next]})
                deg[e_to_v[e1_next]] += 1
        for i in range(len(self.degreeOfEdge)):
            if self.degreeOfEdge[i][0] > 1:
                if self.degreeOfEdge[i][0] != deg[e_to_v[i]]:
                    raise Exception(f"Degree of {i} is {self.degreeOfEdge[i][0]}, but supposed to be {deg[e_to_v[i]]}")

    def checkIsAttachedToSelf(self):
        att_arr = array.array("b", map(lambda x: 0, range(self.cycleCount)))
        for i1 in range(0, self.length, 2):
            i2 = i1 + 1
            c1 = self.currentConf[i1] // 8
            c2 = self.currentConf[i2] // 8
            if c1 == c2:
                att_arr[c1] = 1
        for i in range(len(att_arr)):
            if self.isAttachedToSelf[i] != att_arr[i]:
                raise Exception(f"Wrong entry at index {i} in isAttachedToSelf!!!")

    def checkOccurrencesOfCycles(self):
        oc_arr = array.array("b", map(lambda x: 0, range(self.cycleCount)))
        cooc_arr = array.array("b", map(lambda x: 0, range(self.cycleCount ** 2)))
        for i1 in range(0, self.length, 2):
            i2 = i1 + 1
            c1 = self.currentConf[i1] // 8
            c2 = self.currentConf[i2] // 8
            oc_arr[c1] += 1
            if c2 != c1:
                oc_arr[c2] += 1
            cooc_arr[c1 * self.cycleCount + c2] += 1
        for i in range(len(oc_arr)):
            if self.occurrencesOfCycle[i] != oc_arr[i]:
                raise Exception(f"False entry at index {i} in occurrencesOfCycle")
        for i in range(len(cooc_arr)):
            if self.coOccurrencesOfCycles[i] != cooc_arr[i]:
                raise Exception(f"False entry at index {i} in CoOccurrencesOfCycles")

    def checkLeastFreeEdgeInConf(self):
        least = 0
        while least < self.length and self.usedEdges[least] >= 0:
            least += 1
        if least != self.leastFreeEdge:
            raise Exception(f"LeastFreeEdge is {self.leastFreeEdge}, but it is supposed to be {least}")

    def checkGreatestEdgeAndCycleInConf(self):
        greatest = self.edgeCount - 1
        while greatest > 0 and self.usedEdges[greatest] < 0:
            greatest -= 1
        if greatest != self.greatestEdgeInConf:
            raise Exception(f"greatestEdgeInConf is {self.greatestEdgeInConf}, but it is supposed to be {greatest}")
        if greatest // 8 != self.greatestCycleInConf:
            raise Exception("Greatest cycle is incorrect!!!!")

    def checkEdgeOrderAndUsedEdges(self):
        used_edges_arr = array.array('i', map(lambda x: -1, range(self.edgeCount)))
        leastFree = 0
        for i in range(self.length):
            if used_edges_arr[self.currentConf[i]] >= 0:
                raise Exception(f"Duplication of edge {self.currentConf[i]}!!!!")
            if i % 2 == 0:
                if self.currentConf[i] != leastFree:
                    raise Exception(f"Wrong edge at index {i}!!!!")
                if self.currentConf[i] > self.currentConf[i + 1]:
                    raise Exception(f"Edge pair at indices {(i, i + 1)} in wrong order!!!!")
            used_edges_arr[self.currentConf[i]] = 0
            while used_edges_arr[leastFree] >= 0 and leastFree < self.length - 1:
                leastFree += 1

    def checkDuplicateEdges(self):
        for e in range(self.edgeCount):
            count = 0
            for i in range(self.length):
                if self.currentConf[i] == e:
                    count += 1
            if count > 1:
                raise Exception(f"edge {e} appears too many times!!!!")

    # Main generator
    def generator(self):
        cutoff = 2 * self.maxLength // 3
        while True:
            if self.timeout > 0 and time.time() >= self.timeout:
                self.timed_out = True
                break
            # self.checkConf()
            isOK = self.augment()
            if isOK and self.currentConf[1] != 5:
                if self.length <= cutoff and self.isCanonical():
                    self.output()
                    # print(f"CAN: {self.currentConf[:self.length]}")
                elif self.length > cutoff:
                    self.output()
                else:
                    # print(f"NOT: {self.currentConf[:self.length]}")
                    self.backtrack()
            else:
                self.backtrack()
            if self.breakCondition():
                break
        if self.printToFile:
            self.out_stream.close()

    # Generator of starting values for parallelization
    def seed_generator(self, seed_length):
        while True:
            isOK = self.augment()
            if isOK and self.currentConf[1] != 5 and self.length <= seed_length:
                if self.isCanonical():
                    if self.length >= seed_length:
                        yield list(self.currentConf[:seed_length])
                else:
                    self.backtrack()
            else:
                self.backtrack()
            if self.length < 0:
                break

    # Generator of starting values for parallelization
    def write_seeds(self, seed_length, f_out):
        f = open(f_out, "wb")
        while True:
            isOK = self.augment()
            if isOK and self.currentConf[1] != 5 and self.length <= seed_length:
                if self.isCanonical():
                    if self.length >= seed_length:
                        f.write(bytes(self.currentConf))
                else:
                    self.backtrack()
            else:
                self.backtrack()
            if self.length < 0:
                break
        f.close()
